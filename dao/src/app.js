// Import pre-installed modules
// const https = require('https')
// const path = require('path')
// const fs = require('fs')

// Import downloaded modules
const cors = require('cors')
const express = require('express')

// Import routers
const routerUser = require('./routers/user')
const routerArticle = require('./routers/article')

// Import functions
const startIfNotStarted = require('./db/startIfNotStarted')

// Constants
clientData = {
    host: process.env.DB_HOST || 'localhost',
    port: process.env.DB_PORT || 5432,
    database: process.env.DB_NAME,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
}

// Global constants
global.JWT_EXPIRES_IN = process.env.JWT_EXPIRES_IN || '12h'
global.TIMEZONE = process.env.TIMEZONE || 'UTC'

// Connected to database
startIfNotStarted(clientData)

// Create Express.js app
const app = express()

// App Variables
app.set('AppName', 'Data Access Object (DAO)')

// Define express config
app.use(express.urlencoded({ extended: false }))
app.use(express.json()) // To parse the incoming requests with JSON payloads

// Allow-Control-Allow-Origin for the web browser
app.use(cors())

// Use routers
app.use('/article', routerArticle)
app.use('/user', routerUser)

// HTTPS Server
// const server = https.createServer(
//     {
//         key: fs.readFileSync(path.join(__dirname, '../cert', 'key.pem')),
//         cert: fs.readFileSync(path.join(__dirname, '../cert', 'cert.pem'))
//     }, 
//     app
// )

// module.exports = server
module.exports = app