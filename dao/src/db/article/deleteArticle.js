const text = 'DELETE FROM article WHERE id=$1;'

const deleteArticle = async (id) => {

  try {

    const query = {
      text,
      values: [id],
    }

    const result = await client.query(query)

    return { success: result.rowCount > 0, data: result.rowCount }

  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = deleteArticle