

const text = `select article.*, users.name, users.url
    FROM article 
    left join users on article.user_id=users.id
    WHERE article.id=$1;`

const readArticle = async (id) => {

  try {

    const query = {
      text,
      values: [id],
    }

    const result = await client.query(query)

    return { success: true, data: result.rows[0] }
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = readArticle

// SQL JOIN:
// https://www.w3schools.com/sql/sql_join.asp

// Here this is a LEFT JOIN:
// https://www.w3schools.com/sql/img_leftjoin.gif