const text = 'SELECT * FROM article WHERE user_id=$1;'

const readArticleAll = async (id) => {

  try {

    const query = {
      text,
      values: [id],
    }

    const result = await client.query(query)

    return { success: true, data: result.rows }
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = readArticleAll