const text = "SELECT * FROM article WHERE content LIKE $1 OR title LIKE CONCAT('%', $1, '%');"

const readArticle = async (tag) => {
  
  try {

    const query = {
      text,
      values: [tag],
    }

    const result = await client.query(query)

    return { success: true, data: result.rows }
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = readArticle

// How to make a like search in postgresql and node js:
// https://stackoverflow.com/questions/19471756/how-to-make-a-like-search-in-postgresql-and-node-js

// '%' || $1 || '%'
// https://stackoverflow.com/questions/45039760/error-bind-message-supplies-1-parameters-but-prepared-statement-requires-0