const text = 'SELECT * FROM article WHERE is_completed=true ORDER BY created_at DESC LIMIT $1;'

const readArticle = async (number) => {

  try {

    const query = {
      text,
      values: [number],
    }

    const result = await client.query(query)

    return { success: true, data: result.rows }

  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = readArticle