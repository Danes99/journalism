const updateArticle = async (id, title, content, is_completed) => {
  try {

    const initialArrayOfQueries = [
      {
        test: title,
        text: 'UPDATE article SET title=$2 WHERE id=$1;',
        values: [id, title],
      },
      {
        test: content,
        text: 'UPDATE article SET content=$2 WHERE id=$1;',
        values: [id, content],
      },
      {
        test: is_completed,
        text: 'UPDATE article SET is_completed=$2 WHERE id=$1;',
        values: [id, is_completed],
      }
    ]

    const arrayOfQueriesFiltered = initialArrayOfQueries.filter(element => typeof element.test !== 'undefined')

    const resultArray = await Promise.all(
      arrayOfQueriesFiltered.map(element => client.query(element))
    )

    const success = resultArray.length > 0 ?
      resultArray.every(element => element.rowCount === 1)
      :
      true

    return { success }
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = updateArticle

// https://stackoverflow.com/questions/55225272/map-function-with-async-await