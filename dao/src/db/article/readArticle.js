const text = 'SELECT * FROM article WHERE id=$1;'

const readArticle = async (id) => {

  try {

    const query = {
      text,
      values: [id],
    }

    const result = await client.query(query)

    return { success: true, data: result.rows[0] }
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = readArticle