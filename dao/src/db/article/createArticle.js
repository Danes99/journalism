const text = 'INSERT INTO article(user_id, title, content, is_completed) VALUES ($1, $2, $3, $4);'

const createArticle = async (user_id, title, content, is_completed=false) => {

  try {

    const query = {
      text,
      values: [user_id, title, content, is_completed],
    }

    const result = await client.query(query)

    return { success: result.rowCount > 0, data: 'success' }
  }
  catch (error) {
    return { success: false, data: error }
  }
}

module.exports = createArticle