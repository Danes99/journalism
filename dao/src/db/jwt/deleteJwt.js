const text = `call procedure_delete_jwt($1);`

deleteJwt = async (token) => {

  try {

    const query = {
      text,
      values: [token],
    }

    const result = await client.query(query)

    return { success: true, data: 'success' }
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = deleteJwt