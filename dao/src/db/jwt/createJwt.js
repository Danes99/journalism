const createJwt = async (user_id, token) => {

  try {
    
    const text = `call procedure_create_jwt($1, $2, NOW() AT TIME ZONE '${TIMEZONE}' + INTERVAL '${JWT_EXPIRES_IN}' )`

    const query = {
      text,
      values: [user_id, token],
    }

    const result = await client.query(query)

    return { success: true, data: 'success' }
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = createJwt