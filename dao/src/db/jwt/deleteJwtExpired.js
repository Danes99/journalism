const deleteJwtExpired = async () => {

  try {
    
    const text = `DELETE from jwt WHERE expired_at < NOW() AT TIME ZONE '${TIMEZONE}';`

    const query = {
      text
    }

    const result = await client.query(query)

    return { success: true, data: 'success' }
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = deleteJwtExpired