const text = 'SELECT * FROM jwt WHERE token=$1'

const readJwtFromToken = async token => {

  try {

    const query = {
      text,
      values: [token],
    }

    const result = await client.query(query)

    return { success: result.rows.length > 0, data: result.rows[0] }
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = readJwtFromToken