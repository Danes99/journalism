
const text = 'DELETE FROM jwt WHERE user_id=$1'

const deleteJwtS = async (id) => {

  try {

    const query = {
      text,
      values: [id],
    }

    const result = await client.query(query)

    return { success: true, data: result.rowCount }

  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = deleteJwtS