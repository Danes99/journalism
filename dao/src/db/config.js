module.exports = {

  // Table names
  TABLE_NAME_ARTICLE: 'article',
  TABLE_NAME_JWT: 'jwt',
  TABLE_NAME_USER: 'users',

  // Table keys: user
  TABLE_KEY_USER_ID: 'id',
  TABLE_KEY_USER_NAME: 'name',
  TABLE_KEY_USER_URL: 'url',
  TABLE_KEY_USER_DESCRIPTION: 'description',
  TABLE_KEY_USER_EMAIL: 'email',
  TABLE_KEY_USER_PASSWORD: 'password',
  TABLE_KEY_USER_CREATED_AT: 'created_at',
  TABLE_KEY_USER_UPDATED_AT: 'updated_at',
  TABLE_KEY_USER_IS_ACTIVE: 'is_active',

  // Table keys: jwt
  TABLE_KEY_JWT_ID: 'id',
  TABLE_KEY_JWT_USER_ID: 'user_id',
  TABLE_KEY_JWT_TOKEN: 'token',
  TABLE_KEY_JWT_CREATED_AT: 'created_at',
  TABLE_KEY_JWT_EXPIRED_AT: 'expired_at',

  // Table keys: article
  TABLE_KEY_ARTICLE_ID: 'id',
  TABLE_KEY_ARTICLE_USER_ID: 'user_id',
  TABLE_KEY_ARTICLE_TITLE: 'title',
  TABLE_KEY_ARTICLE_CONTENT: 'content',
  TABLE_KEY_ARTICLE_IS_COMPLETED: 'is_completed',
  TABLE_KEY_ARTICLE_CREATED_AT: 'created_at',
  TABLE_KEY_ARTICLE_UPDATED_AT: 'updated_at'

}
