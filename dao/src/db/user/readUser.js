const text = 'SELECT name, url, email, description, created_at, updated_at FROM users WHERE id=$1'

const readUser = async (id) => {

  try {

    const query = {
      text,
      values: [id],
    }

    const result = await client.query(query)

    return { success: true, data: result.rows[0] }
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = readUser