const text = 'SELECT * FROM users WHERE name=$1 OR email=$2'

const readUserFromEmailOrName = async (name, email) => {

  try {

    const query = {
      text,
      values: [name, email],
    }

    const result = await client.query(query)

    return { success: true, data: result.rows[0] }
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = readUserFromEmailOrName