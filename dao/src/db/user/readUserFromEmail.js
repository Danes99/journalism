const text = 'SELECT * FROM users WHERE email=$1'

const readUserFromEmail = async (email) => {

  try {

    const query = {
      text,
      values: [email],
    }

    const result = await client.query(query)

    return { success: true, data: result.rows[0] }

  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = readUserFromEmail