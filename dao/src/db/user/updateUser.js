const updateUser = async (id, name, email, password) => {

  try {

    const initialArrayOfQueries = [
      {
        test: name,
        text: 'UPDATE users SET name=$2 WHERE id=$1;',
        values: [id, name],
      },
      {
        test: email,
        text: 'UPDATE users SET email=$2 WHERE id=$1;',
        values: [id, email],
      },
      {
        test: password,
        text: 'UPDATE users SET password=$2 WHERE id=$1;',
        values: [id, password],
      }
    ]

    const arrayOfQueriesFiltered = initialArrayOfQueries.filter(element => typeof element.test !== 'undefined')

    const resultArray = await Promise.all(
      arrayOfQueriesFiltered.map(element => client.query(element))
    )

    const success = resultArray.length > 0 ?
      resultArray.every(element => element.rowCount === 1)
      :
      true

    return { success }
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = updateUser

// https://stackoverflow.com/questions/55225272/map-function-with-async-await