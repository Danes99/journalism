const createUser = async (name, email, url, password) => {

  try {

    const arrayOfQueries = [
      {
        text: 'INSERT INTO users (name, email, url, password) VALUES ($1, $2, $3, $4)',
        values: [name, email, url, password],
      },
      {
        text: 'SELECT id FROM users WHERE email=$1;',
        values: [email],
      }
    ]

    const resultArray = await Promise.all(
      arrayOfQueries.map(element => client.query(element))
    )

    // const queryString = `call procedure_create_user('${name}', '${url}', '${email}', '${email}');
    //   select id FROM users WHERE email='${email}';`

    // const result = await client.query(queryString)
    // const resultFromSelect = result[1]

    const resultFromSelect = resultArray[1]

    return { success: resultFromSelect.rows.length > 0, data: resultFromSelect.rows[0] }
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = createUser