const deleteUser = async (id) => {

  try {

    const arrayOfQueries = [
      {
        text: 'DELETE FROM jwt WHERE user_id=$1;',
        values: [id],
      },
      {
        text: 'DELETE FROM users WHERE id=$1;',
        values: [id],
      }
    ]

    const resultArray = await Promise.all(
      arrayOfQueries.map(element => client.query(element))
    )

    return { success: true, data: resultArray[1].rowCount }
    
  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = deleteUser