const text = 'SELECT name, description, created_at, updated_at FROM users WHERE url=$1'

const readUserFromUrl = async (url) => {

  try {

    const query = {
      text,
      values: [url],
    }

    const result = await client.query(query)

    return { success: true, data: result.rows[0] }

  }
  catch (error) {
    console.log(error)
    return { success: false, data: error }
  }
}

module.exports = readUserFromUrl