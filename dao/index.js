// Server Variables
const port = process.env.PORT || 81
const appName = process.env.APP_NAME || 'Data Access Object (DAO)'

// Import Express.js app
const app = require('./src/app')

// Start server
app.listen(
    port, 
    () => console.log(`${appName} listening on port ${port}!`)
)