# Generate SSL certificate

```bash
openssl genrsa -out key.pem
openssl req -new -key key.pem -out csr.pem
openssl x509 -req -days 3650 -in csr.pem -signkey key.pem -out cert.pem
```

<https://www.youtube.com/watch?v=USrMdBF0zcg>
