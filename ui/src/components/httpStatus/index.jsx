// Import pre-installed modules
import React from 'react'

// Display different result according to the HTTP response status code
const AfterRequestDisplay = (props) => {

  switch (props.status) {

    case 200:
      return <div>Done</div>

    case 201:
      return <div>Created</div>

    case 400:
      return <div>Bad request</div>

    case 401:
      return <div>Unauthorized</div>

    case 404:
      return <div>Not found</div>

    case 500:
      return <div>Server Error</div>

    default:
      return <div>Error</div>
  }
}

export default AfterRequestDisplay