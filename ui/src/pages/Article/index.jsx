// Import pre-installed modules
import React, { useState, useEffect } from 'react'

// Import downloaded modules
import { NavLink } from 'react-router-dom'
import { format, formatDistanceToNow } from 'date-fns'

// Import components
import HttpStatus from '../../components/httpStatus'
import Spinner from '../../components/Spinner'
import WebPage from '../../components/WebPage'

// Import config: DAO endpoints
import { DAO_ENDPOINT_ARTICLE } from '../../config/dao'

// Import config: routes
import {
  ROUTE_ARTICLE_CREATE,
  ROUTE_ARTICLE_READ,
  ROUTE_ARTICLE_UPDATE
} from '../../config/routes'

// Constants
const DAO_ENDPOINT_GET_EVERY_ARTICLES = DAO_ENDPOINT_ARTICLE + 'all/'

// Initial state
const INITIAL_STATE_ARTICLE_LIST = null
const INITIAL_STATE_DO_DISPLAY_DELETE_MODAL = false
const INITIAL_SATE_ITEM_TO_DELETE = null

// Initial State: HTTP GET Request (read article list)
const INITIAL_STATE_FETCH_REQUEST_RESPONSE = null

// Initial state: HTTP DELETE Request (delete article)
const INITIAL_STATE_HAS_DELETE_REQUEST_BEEN_MADE = false
const INITIAL_STATE_DELETE_REQUEST_RESPONSE = null

// Date functions
const displayCreatedAt = (date) => date ? format(new Date(date), 'LLLL d, yyyy, h:m aaa') : 'null'
const displayUpdatedAt = (date) => date ? formatDistanceToNow(new Date(date), { addSuffix: true }) : 'null'

const Page = () => {

  // State
  const [articleList, setArticleList] = useState(INITIAL_STATE_ARTICLE_LIST)
  const [doDisplayDeleteModal, setDoDisplayDeleteModal] = useState(INITIAL_STATE_DO_DISPLAY_DELETE_MODAL)
  const [itemToDelete, setItemToDelete] = useState(INITIAL_SATE_ITEM_TO_DELETE)

  // HTTP GET Request (Read layout list)
  const [fetchRequestResponse, setFetchRequestResponse] = useState(INITIAL_STATE_FETCH_REQUEST_RESPONSE)

  // State: HTTP DELETE Request (delete layout)
  const [hasDeleteRequestBeenMade, setHasDeleteRequestBeenMade] = useState(INITIAL_STATE_HAS_DELETE_REQUEST_BEEN_MADE)
  const [deleteRequestResponse, setDeleteRequestResponse] = useState(INITIAL_STATE_DELETE_REQUEST_RESPONSE)

  // HTTP GET Request
  const [fetchArticles] = useState(() => async () => {

    try {

      const requestOptions = { headers: { 'Authorization': window.localStorage.getItem('jwt') } }

      // Fetch article list
      const response = await fetch(DAO_ENDPOINT_GET_EVERY_ARTICLES, requestOptions)
      const status = response.status

      // Status OK: Article deleted
      if (status === 200) {
        const body = await response.json()
        setArticleList(body)
      }

      setFetchRequestResponse(status)

    } catch (error) {
      console.log(error)
    }
  })

  // Delete Article
  // HTTP DELETE Request to DAO
  const [deleteArticle] = useState(() => async (id) => {

    try {

      const requestOptions = {
        headers: { 'Authorization': window.localStorage.getItem('jwt') },
        method: 'DELETE'
      }

      // HTTP DELETE Response from DAO
      const response = await fetch(DAO_ENDPOINT_ARTICLE + id, requestOptions)
      const status = response.status

      setDeleteRequestResponse(status)
      setHasDeleteRequestBeenMade(true)

      // Status OK: Article has been deleted
      if (status === 200) fetchArticles()

    } catch (error) {
      console.log(error)
    }
  })

  const [startDeleteProcedure] = useState(() => item => {

    // Change the value for the deletion
    // The value is used for :
    // 1. Display in the modal
    // 2. Parameters for the HTTP DELETE Request
    setItemToDelete(item)

    // Display delete modal
    setDoDisplayDeleteModal(true)
  })

  const [cancelDeleteProcedure] = useState(() => () => {
    setItemToDelete(INITIAL_SATE_ITEM_TO_DELETE)
    setDoDisplayDeleteModal(false)
  })

  const [finishDeleteProcedure] = useState(() => () => {
    setHasDeleteRequestBeenMade(INITIAL_STATE_HAS_DELETE_REQUEST_BEEN_MADE)
    setDoDisplayDeleteModal(INITIAL_STATE_DO_DISPLAY_DELETE_MODAL)
    setItemToDelete(INITIAL_SATE_ITEM_TO_DELETE)
  })

  useEffect(() => {
    fetchArticles()
    // Do not delete the following comment, it disables the useEffect warning on the console
    // eslint-disable-next-line react-hooks/exhaustive-deps 
  }, [])

  return <WebPage title='Articles'>

    {/* Create new item */}
    <div className='transition duration-500 bg-blue-500 hover:bg-blue-600 transform hover:scale-110 text-white font-bold mb-3 py-2 px-4 border rounded-full'>
      <NavLink to={ROUTE_ARTICLE_CREATE}>New +</NavLink>
    </div>

    {/* Modal + Backdrop */}
    {/* fixed h-screen w-screen */}
    {doDisplayDeleteModal ?
      <div className='flex items-center justify-center transition duration-500'>

        {/* Backdrop */}
        <div className='transition duration-500 absolute inset-0 bg-gray-200 bg-opacity-75 z-40'>
        </div>

        {/* Modal */}

        {/* Nested Modal */}

        {hasDeleteRequestBeenMade ?
          deleteRequestResponse ?
            <div className='transition duration-500 fixed max-w-xl w-full bg-white shadow-lg z-50 rounded-lg overflow-hidden'>
              <div className='p-4 flex space-x-4 md:flex-row flex-col md:text-left text-center items-center'>
                <div className='bg-red-50 p-3 md:self-start rounded-full'>
                  <svg xmlns='http://www.w3.org/2000/svg' className='w-6 h-6 fill-current text-red-700' width='24' height='24' viewBox='0 0 24 24'><path d='M12 5.177l8.631 15.823h-17.262l8.631-15.823zm0-4.177l-12 22h24l-12-22zm-1 9h2v6h-2v-6zm1 9.75c-.689 0-1.25-.56-1.25-1.25s.561-1.25 1.25-1.25 1.25.56 1.25 1.25-.561 1.25-1.25 1.25z' /></svg>
                </div>
                <div>
                  <h1 className='text-xl font-semibold tracking-wide text-red-700'>Delete request result</h1>
                  <p className='text-gray-500'>
                    {deleteRequestResponse === 200 ? 'Success' : <HttpStatus status={deleteRequestResponse} />}
                  </p>
                </div>
              </div>
              <div className='p-3 bg-gray-50 text-right md:space-x-4 md:block flex flex-col-reverse'>
                {/* <button
                    className='px-4 md:py-1.5 py-2 bg-white border-2 rounded-lg focus:ring-offset-2 focus:outline-none focus:ring-2 focus:ring-blue-800 hover:bg-gray-50'             
                    >Cancel</button> */}
                <button
                  className='mb-2 md:mb-0 px-4 md:py-1.5 py-2 bg-red-700 text-white rounded-lg focus:ring-offset-2 focus:outline-none focus:ring-2 focus:ring-red-800 hover:bg-red-800'
                  onClick={(e) => finishDeleteProcedure()}
                >OK</button>
              </div>
            </div>
            : <Spinner />
          :

          <div className='transition duration-500 fixed max-w-xl w-full bg-white shadow-lg z-50 rounded-lg overflow-hidden'>
            <div className='p-4 flex space-x-4 md:flex-row flex-col md:text-left text-center items-center'>
              <div className='bg-red-50 p-3 md:self-start rounded-full'>
                <svg xmlns='http://www.w3.org/2000/svg' className='w-6 h-6 fill-current text-red-700' width='24' height='24' viewBox='0 0 24 24'><path d='M12 5.177l8.631 15.823h-17.262l8.631-15.823zm0-4.177l-12 22h24l-12-22zm-1 9h2v6h-2v-6zm1 9.75c-.689 0-1.25-.56-1.25-1.25s.561-1.25 1.25-1.25 1.25.56 1.25 1.25-.561 1.25-1.25 1.25z' /></svg>
              </div>
              <div>
                <h1 className='text-xl font-semibold tracking-wide text-red-700'>Delete Dashboard</h1>
                <p className='text-gray-500'>Are you sure you want to delete '{articleList[itemToDelete].title}'? The dashboard will be permanently removed. This action cannot be undone.</p>
              </div>
            </div>
            <div className='p-3 bg-gray-50 text-right md:space-x-4 md:block flex flex-col-reverse'>
              <button
                className='px-4 md:py-1.5 py-2 bg-white border-2 rounded-lg focus:ring-offset-2 focus:outline-none focus:ring-2 focus:ring-blue-800 hover:bg-gray-50'
                onClick={(e) => cancelDeleteProcedure()}
              >Cancel</button>
              <button
                className='mb-2 md:mb-0 px-4 md:py-1.5 py-2 bg-red-700 text-white rounded-lg focus:ring-offset-2 focus:outline-none focus:ring-2 focus:ring-red-800 hover:bg-red-800'
                onClick={(e) => deleteArticle(articleList[itemToDelete].id)}
              >Delete</button>
            </div>
          </div>
        }
      </div>
      : null
    }

    {fetchRequestResponse ?
      fetchRequestResponse === 200 ?
        articleList ?

          <div className='overflow-x-auto'>
            <div className='w-full lg:w-6/6'>
              <div className='bg-white shadow-md rounded my-6'>

                {/* Article list */}
                <table className='min-w-max w-full table-auto'>

                  {/* Article list header */}
                  <thead className='bg-gray-200 text-gray-600 uppercase text-sm leading-normal'>
                    <tr>
                      <th className='py-3 px-6 text-left' >Title</th>
                      <th className='py-3 px-6 text-left' >Created</th>
                      <th className='py-3 px-6 text-left' >Updated</th>
                      <th className='py-3 px-6 text-center'>Status</th>
                      <th className='py-3 px-6 text-center' >Actions</th>
                    </tr>
                  </thead>

                  {/* Article list body */}
                  <tbody className='text-gray-600 text-sm font-light'>
                    {articleList.map(
                      (item, index) => <tr
                        key={`article_${item.id}`}
                        className='transition duration-300 border-b border-gray-200 hover:bg-gray-100'
                      >

                        {/* Article title */}
                        <td className='py-3 px-6 text-left whitespace-nowrap'>
                          <div className='flex items-center'>

                            {/* Article title as plain text */}
                            <span className='font-medium'>{item.title}</span>
                          </div>
                        </td>

                        {/* Article Created at */}
                        <td className='py-3 px-6 text-left'>
                          <div className='flex items-center'>
                            <span>{
                              item.created_at ? displayCreatedAt(item.created_at) : 'null'
                            }</span>
                          </div>
                        </td>

                        {/* Article Updated at */}
                        <td className='py-3 px-6 text-left'>
                          <div className='flex items-center'>
                            <span>{
                              item.updated_at ? displayUpdatedAt(item.updated_at) : 'null'
                            }</span>
                          </div>
                        </td>

                        {/* Is article completed? */}
                        <td className='py-3 px-6 text-center'>
                          {item.is_completed ?
                            <span className='bg-green-200 text-green-600 py-1 px-3 rounded-full text-xs'>Completed</span>
                            :
                            <span className='bg-red-200 text-red-600 py-1 px-3 rounded-full text-xs'>Pending</span>}
                        </td>

                        {/* Actions menu */}
                        <td className='py-3 px-6 text-center'>
                          <div className='flex items-center justify-center'>

                            {/* Read article (Eye) */}
                            <div className='transition duration-300 w-4 mr-2 transform hover:text-purple-500 hover:scale-110'>
                              <NavLink to={ROUTE_ARTICLE_READ + item.id}>
                                <svg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 24 24' stroke='currentColor'>
                                  <path strokeLinecap='round' strokeLinejoin='round' strokeWidth='2' d='M15 12a3 3 0 11-6 0 3 3 0 016 0z' />
                                  <path strokeLinecap='round' strokeLinejoin='round' strokeWidth='2' d='M2.458 12C3.732 7.943 7.523 5 12 5c4.478 0 8.268 2.943 9.542 7-1.274 4.057-5.064 7-9.542 7-4.477 0-8.268-2.943-9.542-7z' />
                                </svg>
                              </NavLink>
                            </div>

                            {/* Edit article (Pencil) */}
                            <div className='transition duration-300 w-4 mr-2 transform hover:text-yellow-500 hover:scale-110'>
                              <NavLink to={ROUTE_ARTICLE_UPDATE + item.id}>
                                <svg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 24 24' stroke='currentColor'>
                                  <path strokeLinecap='round' strokeLinejoin='round' strokeWidth='2' d='M15.232 5.232l3.536 3.536m-2.036-5.036a2.5 2.5 0 113.536 3.536L6.5 21.036H3v-3.572L16.732 3.732z' />
                                </svg>
                              </NavLink>
                            </div>

                            {/* Delete article (Trash) */}
                            <div
                              className='transition duration-300 w-4 mr-2 transform hover:text-red-500 hover:scale-110'
                              onClick={() => startDeleteProcedure(index)}
                            >

                              <svg xmlns='http://www.w3.org/2000/svg' fill='none' viewBox='0 0 24 24' stroke='currentColor'>
                                <path strokeLinecap='round' strokeLinejoin='round' strokeWidth='2' d='M19 7l-.867 12.142A2 2 0 0116.138 21H7.862a2 2 0 01-1.995-1.858L5 7m5 4v6m4-6v6m1-10V4a1 1 0 00-1-1h-4a1 1 0 00-1 1v3M4 7h16' />
                              </svg>
                            </div>
                          </div>
                        </td>

                      </tr>
                    )}
                  </tbody>

                </table>
              </div>
            </div>
          </div>
          :
          <p className='block font-sans'>Error</p>

        : <HttpStatus status={fetchRequestResponse} />
      :
      <Spinner />
    }

  </WebPage>
}

export default Page

// Table: https://tailwindcomponents.com/component/projects-table
// Modal: https://tailwindcomponents.com/component/simple-alert-modal
