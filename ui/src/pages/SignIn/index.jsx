// Import pre-installed modules
import React, { useState } from 'react'

// Import downloaded modules
import { NavLink, useHistory } from 'react-router-dom'

// Import components
import HttpStatus from '../../components/httpStatus'
import Spinner from '../../components/Spinner'

// Import functions
import {
  testIsUserEmailValid,
  testIsUserPasswordValid
} from '../../utils/isUserValid'

// Import config: DAO endpoints
import { DAO_ENDPOINT_USER_LOGIN } from '../../config/dao'

// Import config: routes
import { ROUTE_ARTICLE, ROUTE_REGISTER } from '../../config/routes'

// Import SVG
import logo from '../../svg/workflow-mark-indigo-600.svg'

// Initial state: user
const INITIAL_STATE_USER_EMAIL = ''
const INITIAL_STATE_USER_PASSWORD = ''
const INITIAL_STATE_IS_USER_EMAIL_VALID = false
const INITIAL_STATE_IS_USER_PASSWORD_VALID = false

// Initial state: HTTP POST Request
const INITIAL_STATE_HAS_POST_REQUEST_BEEN_MADE = false
const INITIAL_STATE_POST_REQUEST_RESPONSE = null

const Page = (props) => {

  // History
  const [history] = useState(useHistory())

  // State
  const [userEmail, setUserEmail] = useState(INITIAL_STATE_USER_EMAIL)
  const [userPassword, setUserPassword] = useState(INITIAL_STATE_USER_PASSWORD)
  const [isUserEmailValid, setIsUserEmailValid] = useState(INITIAL_STATE_IS_USER_EMAIL_VALID)
  const [isUserPasswordValid, setIsUserPasswordValid] = useState(INITIAL_STATE_IS_USER_PASSWORD_VALID)

  // State: HTTP POST Request
  const [hasPostRequestBeenMade, setHasPostRequestBeenMade] = useState(INITIAL_STATE_HAS_POST_REQUEST_BEEN_MADE)
  const [postRequestResponse, setPostRequestResponse] = useState(INITIAL_STATE_POST_REQUEST_RESPONSE)

  // Update User Email
  const [handleChangeUserEmail] = useState(() => (e) => {
    setUserEmail(e.currentTarget.value)
    setIsUserEmailValid(testIsUserEmailValid(e.currentTarget.value))
  })

  // Update User Password
  const [handleChangeUserPassword] = useState(() => (e) => {
    setUserPassword(e.currentTarget.value)
    setIsUserPasswordValid(testIsUserPasswordValid(e.currentTarget.value))
  })

  // Request: HTTP POST Login
  const [handleSubmit] = useState(() => async (email, password) => {

    try {

      setHasPostRequestBeenMade(true)

      const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ email, password }, null, 4)
      }

      // Send HTTP POST request: login user
      const response = await fetch(DAO_ENDPOINT_USER_LOGIN, requestOptions)
      const status = response.status

      if (status === 200) {

        const body = await response.json()

        props.tokenReceived(body.token)
        history.push(ROUTE_ARTICLE)

      } else {
        setPostRequestResponse(status)
      }

    } catch (error) {
      console.log(error)
    }

  })

  const canSubmit = isUserEmailValid && isUserPasswordValid

  return (
    <div className='flex items-center justify-center py-12 px-4 sm:px-6 lg:px-8'>


      {hasPostRequestBeenMade ?
        postRequestResponse ?
          <HttpStatus status={postRequestResponse} />
          :
          <Spinner />
        :

        <div className='max-w-md w-full space-y-8'>

          {/* Sign In message */}
          <div>
            <img className='mx-auto h-12 w-auto' src={logo} alt='Workflow' />
            <h2 className='mt-6 text-center text-3xl font-extrabold text-gray-900'>Sign in to your account</h2>
            <div className='mt-2 text-center text-sm text-gray-600'>
              Or&nbsp;
              <p className='transition duration-500 font-medium text-indigo-600 hover:text-indigo-500'>
                <NavLink to={ROUTE_REGISTER}>Sign Up</NavLink>
              </p>
            </div>
          </div>

          {/* Sign In Form */}
          <div className='mt-8 space-y-6' >
            <input type='hidden' name='remember' value='true' />

            {/* Email & Password */}
            <div className='rounded-md shadow-sm -space-y-px'>

              {/* Email */}
              <div>
                <label htmlFor='email-address' className='sr-only'>Email address</label>
                <input id='email-address' name='email' type='email' autoComplete='email' placeholder='Email address' required
                  onChange={handleChangeUserEmail}
                  className='appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-t-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm' />
              </div>

              {/* Password */}
              <div>
                <label htmlFor='password' className='sr-only'>Password</label>
                <input id='password' name='password' type='password' autoComplete='current-password' placeholder='Password' required
                  onChange={handleChangeUserPassword}
                  className='appearance-none rounded-none relative block w-full px-3 py-2 border border-gray-300 placeholder-gray-500 text-gray-900 rounded-b-md focus:outline-none focus:ring-indigo-500 focus:border-indigo-500 focus:z-10 sm:text-sm' />
              </div>

            </div>

            <div className='flex items-center justify-between'>
              <div className='flex items-center'>
                <input id='remember_me' name='remember_me' type='checkbox' className='h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded' />
                <label htmlFor='remember_me' className='ml-2 block text-sm text-gray-900'>Remember me</label>
              </div>
              <div className='text-sm'>
                <p className='transition duration-500 font-medium text-indigo-600 hover:text-indigo-500'>Forgot your password?</p>
              </div>
            </div>

            {/* Sign in button */}
            <button onClick={() => handleSubmit(userEmail, userPassword)} disabled={!canSubmit}
              className='transition duration-500 group relative w-full flex justify-center py-2 px-4 border border-transparent text-sm font-medium rounded-md text-white bg-indigo-600 hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500'
            >
              <span className='absolute left-0 inset-y-0 flex items-center pl-3'>
                {/* SVG Lock */}
                <svg className='transition duration-500 h-5 w-5 text-indigo-500 group-hover:text-indigo-400' xmlns='http://www.w3.org/2000/svg' viewBox='0 0 20 20' fill='currentColor' aria-hidden='true'>
                  <path fillRule='evenodd' d='M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z' clipRule='evenodd' />
                </svg>
              </span>Sign in
            </button>

          </div>

        </div>

      }

    </div>
  )
}

export default Page

// Sign In form: https://tailwindui.com/components/application-ui/forms/sign-in-forms