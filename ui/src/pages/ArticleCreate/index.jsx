// Import pre-installed modules
import React, { useState } from 'react'

// Import components
import ArticleForm from '../../components/ArticleForm'
import HttpStatus from '../../components/httpStatus'
import Spinner from '../../components/Spinner'
import WebPage from '../../components/WebPage'

// Import functions
import {
  testIsArticleTitleValid,
  testIsArticleContentValid
} from '../../utils/isArticleValid'

// Import Config
import { DAO_ENDPOINT_ARTICLE } from '../../config/dao'

// Initial state
const INITIAL_STATE_ARTICLE_TITLE = ''
const INITIAL_STATE_ARTICLE_CONTENT = ''
const INITIAL_STATE_ARTICLE_IS_COMPLETED = false
const INITIAL_STATE_IS_ARTICLE_TITLE_VALID = false
const INITIAL_STATE_IS_ARTICLE_CONTENT_VALID = false
const INITIAL_STATE_HAS_POST_REQUEST_BEEN_MADE = false
const INITIAL_STATE_POST_REQUEST_RESPONSE = null

const Page = () => {

  // State
  const [articleTitle, setArticleTitle] = useState(INITIAL_STATE_ARTICLE_TITLE)
  const [articleContent, setArticleContent] = useState(INITIAL_STATE_ARTICLE_CONTENT)
  const [articleIsCompleted, setArticleIsCompleted] = useState(INITIAL_STATE_ARTICLE_IS_COMPLETED)

  const [isArticleTitleValid, setIsArticleTitleValid] = useState(INITIAL_STATE_IS_ARTICLE_TITLE_VALID)
  const [isArticleContentValid, setIsArticleContentValid] = useState(INITIAL_STATE_IS_ARTICLE_CONTENT_VALID)

  const [hasPostRequestBeenMade, setHasPostRequestBeenMade] = useState(INITIAL_STATE_HAS_POST_REQUEST_BEEN_MADE)
  const [postRequestResponse, setPostRequestResponse] = useState(INITIAL_STATE_POST_REQUEST_RESPONSE)

  // Handle update of: article title
  const [handleChangeArticleTitle] = useState(() => (e) => {

    setArticleTitle(e.currentTarget.value)
    setIsArticleTitleValid(testIsArticleTitleValid(e.currentTarget.value))
  })

  // Handle update of: article content
  const [handleChangeArticleContent] = useState(() => (e) => {

    setArticleContent(e.currentTarget.value)
    setIsArticleContentValid(testIsArticleContentValid(e.currentTarget.value))
  })

  // Handle update of: article 'is completed?'
  const [handleChangeArticleIsCompleted] = useState(() => (e) => {

    setArticleIsCompleted(e.currentTarget.value)
  })

  // Handle submit article
  const [handleSubmit] = useState(() => async (title, content, is_completed) => {

    try {

      setHasPostRequestBeenMade(true)

      const requestOptions = {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': window.localStorage.getItem('jwt')
        },
        body: JSON.stringify(
          { title, content, is_completed },
          null,
          4
        )
      }

      // Send HTTP POST Request: create article
      const response = await fetch(DAO_ENDPOINT_ARTICLE, requestOptions)

      setPostRequestResponse(response.status)

    } catch (error) {
      console.log(error)
    }

  })

  // Can submit article?
  const canSubmit = isArticleTitleValid && isArticleContentValid

  return (
    <WebPage title='New Post'>

      {hasPostRequestBeenMade ?
        <div className='py-10'>
          {postRequestResponse ?
            <HttpStatus status={postRequestResponse} />
            :
            <Spinner />
          }
        </div>
        :
        <ArticleForm
          handleChangeArticleTitle={handleChangeArticleTitle}
          handleChangeArticleContent={handleChangeArticleContent}
          handleChangeArticleIsCompleted={handleChangeArticleIsCompleted}
          handleSubmit={() => handleSubmit(articleTitle, articleContent, articleIsCompleted)}
          canSubmit={canSubmit}
        />
      }

    </WebPage>
  )
}

export default Page
