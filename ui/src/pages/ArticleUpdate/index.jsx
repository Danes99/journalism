// Import pre-installed modules
import React, { useState, useEffect } from 'react'

// Import components
import ArticleForm from '../../components/ArticleForm'
import HttpStatus from '../../components/httpStatus'
import Spinner from '../../components/Spinner'
import WebPage from '../../components/WebPage'

// Import functions
import {
  testIsArticleTitleValid,
  testIsArticleContentValid
} from '../../utils/isArticleValid'

// Import Config
import {
  DAO_ENDPOINT_ARTICLE,
  DAO_ENDPOINT_ARTICLE_ID
} from '../../config/dao'

// Initial state
const INITIAL_STATE_ARTICLE_TITLE = ''
const INITIAL_STATE_ARTICLE_CONTENT = ''
const INITIAL_STATE_ARTICLE_IS_COMPLETED = null
const INITIAL_STATE_IS_ARTICLE_TITLE_VALID = false
const INITIAL_STATE_IS_ARTICLE_CONTENT_VALID = false

const INITIAL_STATE_HAS_PATCH_REQUEST_BEEN_MADE = false
const INITIAL_STATE_PATCH_REQUEST_RESPONSE = null
const INITIAL_STATE_FETCH_REQUEST_RESPONSE = null

const Page = () => {

  // Constants
  const [ARTICLE_ID] = useState(window.location.pathname.split('/')[3])

  // State: article
  const [articleTitle, setArticleTitle] = useState(INITIAL_STATE_ARTICLE_TITLE)
  const [articleContent, setArticleContent] = useState(INITIAL_STATE_ARTICLE_CONTENT)
  const [articleIsCompleted, setArticleIsCompleted] = useState(INITIAL_STATE_ARTICLE_IS_COMPLETED)

  const [isArticleTitleValid, setIsArticleTitleValid] = useState(INITIAL_STATE_IS_ARTICLE_TITLE_VALID)
  const [isArticleContentValid, setIsArticleContentValid] = useState(INITIAL_STATE_IS_ARTICLE_CONTENT_VALID)

  // State: HTTP requests
  const [fetchRequestResponse, setFetchRequestResponse] = useState(INITIAL_STATE_FETCH_REQUEST_RESPONSE)
  const [hasPatchRequestBeenMade, setHasPatchRequestBeenMade] = useState(INITIAL_STATE_HAS_PATCH_REQUEST_BEEN_MADE)
  const [patchRequestResponse, setPatchRequestResponse] = useState(INITIAL_STATE_PATCH_REQUEST_RESPONSE)

  // Handle update of: article title
  const [handleChangeArticleTitle] = useState(() => (e) => {

    setArticleTitle(e.currentTarget.value)
    setIsArticleTitleValid(testIsArticleTitleValid(e.currentTarget.value))
  })

  // Handle update of: article content
  const [handleChangeArticleContent] = useState(() => (e) => {

    setArticleContent(e.currentTarget.value)
    setIsArticleContentValid(testIsArticleContentValid(e.currentTarget.value))
  })

  // Handle update of: article 'is completed?'
  const [handleChangeArticleIsCompleted] = useState(() => (e) => {

    setArticleIsCompleted(e.currentTarget.value)
  })

  // Handle submit article
  const [handleSubmit] = useState(() => async (title, content, is_completed) => {

    try {

      setHasPatchRequestBeenMade(true)

      // HTTP request options
      const requestOptions = {
        method: 'PATCH',
        headers: {
          'Content-Type': 'application/json',
          'Authorization': window.localStorage.getItem('jwt')
        },
        body: JSON.stringify(
          { title, content, is_completed },
          null,
          4
        )
      }

      // Send HTTP POST request: create article
      const response = await fetch(DAO_ENDPOINT_ARTICLE + ARTICLE_ID, requestOptions)
      setPatchRequestResponse(response.status)

    } catch (error) {
      console.log(error)
    }

  })

  const [fetchArticle] = useState(() => async () => {

    try {

      // HTTP GET Request options
      const requestOptions = { headers: { 'Authorization': window.localStorage.getItem('jwt') } }

      // Fetch article list
      const response = await fetch(DAO_ENDPOINT_ARTICLE_ID + ARTICLE_ID, requestOptions)
      const status = response.status

      // Status OK: Article got
      if (status === 200) {

        // Article
        const body = await response.json()

        // Update state: article
        setArticleTitle(body.title)
        setArticleContent(body.content)
        setArticleIsCompleted(body.is_completed)

        // Update state: article validity
        setIsArticleTitleValid(true)
        setIsArticleContentValid(true)
      }

      setFetchRequestResponse(status)

    } catch (error) {
      console.log(error)
    }
  })

  // Can submit article?
  const canSubmit = isArticleTitleValid && isArticleContentValid

  // Run only once
  useEffect(() => {
    fetchArticle()
    // Do not delete the following comment, it disables the useEffect warning on the console
    // eslint-disable-next-line react-hooks/exhaustive-deps 
  }, [])

  return (
    <WebPage title='Update Post'>

      {hasPatchRequestBeenMade ?
        <div className='py-10'>
          {patchRequestResponse ?
            <HttpStatus status={patchRequestResponse} />
            :
            <Spinner />
          }
        </div>
        :
        fetchRequestResponse ?
          fetchRequestResponse === 200 ?
            <ArticleForm

              // Article data
              articleTitle={articleTitle}
              articleContent={articleContent}
              articleIsCompleted={articleIsCompleted}

              // Handle changes
              handleChangeArticleTitle={handleChangeArticleTitle}
              handleChangeArticleContent={handleChangeArticleContent}
              handleChangeArticleIsCompleted={handleChangeArticleIsCompleted}
              handleSubmit={() => handleSubmit(articleTitle, articleContent, articleIsCompleted)}

              // Can submit updates?
              canSubmit={canSubmit}
            />
            :
            <HttpStatus status={fetchRequestResponse} />
          :
          <Spinner />
      }
    </WebPage>
  )
}

export default Page
