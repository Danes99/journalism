// Basics
const ROUTE_HOME = '/'
const ROUTE_ABOUT = '/about'
const ROUTE_HELP = '/help'

// Article
const ROUTE_ARTICLE = '/article'
const ROUTE_ARTICLE_READ = ROUTE_ARTICLE + '/read/'
const ROUTE_ARTICLE_CREATE = ROUTE_ARTICLE + '/create/'
const ROUTE_ARTICLE_UPDATE = ROUTE_ARTICLE + '/update/'

// User
const ROUTE_PROFILE = '/profile'

// Redirect
const ROUTE_REDIRECT_404 = ROUTE_ARTICLE

// Authentication
const ROUTE_LOGIN = '/login'
const ROUTE_REGISTER = '/register'

// Exports
export {

  // Basics
  ROUTE_HOME,
  ROUTE_ABOUT,
  ROUTE_HELP,

  // Dashboard
  ROUTE_ARTICLE,
  ROUTE_ARTICLE_READ,
  ROUTE_ARTICLE_CREATE,
  ROUTE_ARTICLE_UPDATE,

  // Settings
  ROUTE_PROFILE,

  // Redirect
  ROUTE_REDIRECT_404,

  // Authentication
  ROUTE_LOGIN,
  ROUTE_REGISTER
}