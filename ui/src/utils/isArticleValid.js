// Import config: article parameters
import {
  ARTICLE_TITLE_MIN_LENGTH,
  ARTICLE_TITLE_MAX_LENGTH,
  ARTICLE_CONTENT_MIN_LENGTH,
  ARTICLE_CONTENT_MAX_LENGTH
} from '../config/article'

const testIsArticleTitleValid = (value) => {

  const length = value.length

  return (
    length > ARTICLE_TITLE_MIN_LENGTH
    && length < ARTICLE_TITLE_MAX_LENGTH
  )

}

const testIsArticleContentValid = (value) => {

  const length = value.length

  return (
    length > ARTICLE_CONTENT_MIN_LENGTH
    && length < ARTICLE_CONTENT_MAX_LENGTH
  )

}

export {
  testIsArticleTitleValid,
  testIsArticleContentValid
}
