// Import downloaded modules
import validator from 'validator'

const testIsUserNameValid = (value) => {

  return value.length > 1
}

const testIsUserEmailValid = (value) => {

  return validator.isEmail(value)
}

const testIsUserPasswordValid = (value) => {

  return validator.isStrongPassword(value)
}

export {
  testIsUserNameValid,
  testIsUserEmailValid,
  testIsUserPasswordValid
}