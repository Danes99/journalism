// Import pre-installed modules
import { useState, useEffect } from 'react'

// Import downloaded modules
import {
  BrowserRouter as Router,
  Switch,
  Route
} from 'react-router-dom'

// Import components
import Footer from './components/Footer'
import NavBar from './components/Navbar'
import PrivateRoute from './components/PrivateRoute'
import Spinner from './components/Spinner'
import WebPage from './components/WebPage'

// Import pages
import About from './pages/About'
import Article from './pages/Article'
import ArticleCreate from './pages/ArticleCreate'
import ArticleRead from './pages/ArticleRead'
import ArticleUpdate from './pages/ArticleUpdate'
import Help from './pages/Help'
import Home from './pages/Home'
import NotFound404 from './pages/404'
import SignIn from './pages/SignIn'
import SignUp from './pages/SingUp'
import UserProfile from './pages/UserProfile'

// Import config: DAO endpoints
import { DAO_ENDPOINT_USER_IS_LOGGED_IN } from './config/dao'

// Import config: routes
import {
  ROUTE_HOME,
  ROUTE_HELP,
  ROUTE_ABOUT,
  ROUTE_ARTICLE,
  ROUTE_ARTICLE_CREATE,
  ROUTE_ARTICLE_READ,
  ROUTE_ARTICLE_UPDATE,
  ROUTE_PROFILE,
  ROUTE_LOGIN,
  ROUTE_REGISTER
} from './config/routes'

// Initial State
const INITIAL_STATE_HAS_REQUEST_BEEN_MADE = false
const INITIAL_STATE_IS_AUTHENTICATED = false

const App = () => {

  // State
  const [hasRequestBeenMade, setHasRequestBeenMade] = useState(INITIAL_STATE_HAS_REQUEST_BEEN_MADE)
  const [isAuthenticated, setIsAuthenticated] = useState(INITIAL_STATE_IS_AUTHENTICATED)

  const [testIsAuth] = useState(() => async () => {

    // Get jwt from web browser local storage
    // https://developer.mozilla.org/en-US/docs/Web/API/Window/localStorage
    const jwt = window.localStorage.getItem('jwt')

    // Is there a JWT in Web Browser Local Storage?
    if (!jwt) {
      setHasRequestBeenMade(true)
      setIsAuthenticated(false)
      return false
    }

    // Put JWT in HTTP Request Header
    // Test (ask DAO) if JWT is valid
    const requestOptions = { headers: { 'Authorization': jwt } }

    // HTTP GET request
    const response = await fetch(DAO_ENDPOINT_USER_IS_LOGGED_IN, requestOptions)

    setIsAuthenticated(response.status === 200)
    setHasRequestBeenMade(true)
  })

  const [tokenReceived] = useState(() => (token) => {

    // Write JWT in local storage
    window.localStorage.setItem('jwt', token)
    setIsAuthenticated(true)
  })

  const [logOut] = useState(() => () => {

    localStorage.removeItem('jwt')
    setIsAuthenticated(false)
  })

  // Run only once
  useEffect(() => {
    testIsAuth()
    // Do not delete the following comment, it disables the useEffect warning on the console
    // eslint-disable-next-line react-hooks/exhaustive-deps 
  }, [])

  return (
    <div className='flex flex-col min-h-screen'>
      <Router>
        <div className='flex-grow'>

          {/* Navbar */}
          <NavBar
            displayUserMenu={isAuthenticated}
            logout={logOut}
          />

          {hasRequestBeenMade ?

            <Switch>
              {/* A <Switch> looks through its children <Route>s and renders the first one that matches the current URL. */}

              {/* Private routes, do not need to be authenticated to access */}
              <Route path={ROUTE_HOME} exact component={Home} />
              <Route path={ROUTE_HELP} exact component={Help} />
              <Route path={ROUTE_ABOUT} exact component={About} />

              {/* When user is authenticated, use callback function (tokenReceived) */}
              <Route path={ROUTE_LOGIN} exact render={(props) => <SignIn tokenReceived={tokenReceived} />} />
              <Route path={ROUTE_REGISTER} exact render={(props) => <SignUp tokenReceived={tokenReceived} />} />

              {/* Private routes, need to be authenticated to access */}
              <PrivateRoute path={ROUTE_PROFILE} component={UserProfile} isAuthenticated={isAuthenticated} />
              <PrivateRoute path={ROUTE_ARTICLE} exact component={Article} isAuthenticated={isAuthenticated} />
              <PrivateRoute path={ROUTE_ARTICLE_CREATE} exact component={ArticleCreate} isAuthenticated={isAuthenticated} />
              <PrivateRoute path={ROUTE_ARTICLE_READ} component={ArticleRead} isAuthenticated={isAuthenticated} />
              <PrivateRoute path={ROUTE_ARTICLE_UPDATE} component={ArticleUpdate} isAuthenticated={isAuthenticated} />

              {/* 404 */}
              <Route component={NotFound404} />

            </Switch>

            :
            <WebPage title='Loading'>
              {/* Display loading spinner while waiting for the authentication */}
              <Spinner />
            </WebPage>
          }

        </div>

        {/* Footer */}
        <Footer />

      </Router>
    </div>
  );
}

export default App;
