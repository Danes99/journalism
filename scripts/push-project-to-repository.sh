#!/bin/bash
# Does it have execute permissions? Try a chmod +x scriptname and then ./scriptname
clear

# Moving to server directory
cd `dirname "$0"`
cd ..

# Compulsory commit message
while [ -z "$commitMessage" ]
do
  read -p "Please enter a commit message: " commitMessage
done

# Upload the project to the repository
git add .
git commit -m "$commitMessage "
git push

# https://www.onlinetutorialspoint.com/git/step-by-step-how-to-push-the-project-into-git-repository.html