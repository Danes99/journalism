drop table if exists users;

-- Create table users
create table users(
  id SERIAL PRIMARY KEY,
  name VARCHAR(64) NOT NULL UNIQUE,
  url VARCHAR(64) NOT NULL UNIQUE,
  description VARCHAR(5000) NOT NULL DEFAULT 'Hello, this is my description.',
  email VARCHAR(64) NOT NULL UNIQUE,
	password VARCHAR(150),
  created_at timestamp with time zone NOT NULL DEFAULT ( NOW() AT TIME ZONE 'UTC' ),
  updated_at timestamp with time zone NOT NULL DEFAULT ( NOW() AT TIME ZONE 'UTC' ),
  is_active boolean NOT NULL DEFAULT true
);

grant all privileges on table users to postgres;

-- Create a Trigger function
-- to update "updated_at" when UPDATE the SQL table
-- https://www.dbrnd.com/2016/03/postgresql-update-the-timestamp-column-with-the-use-of-trigger/
CREATE OR REPLACE FUNCTION trg_fn_users_updated_at()
RETURNS TRIGGER AS $$
BEGIN
  NEW.updated_at = NOW() AT TIME ZONE 'UTC'; 
  RETURN NEW;
END;
$$ language 'plpgsql';

-- Create an UPDATE TRIGGER
CREATE TRIGGER trg_update_users BEFORE UPDATE
ON users FOR EACH ROW EXECUTE PROCEDURE 
trg_fn_users_updated_at();

-- Store procedures

-- Create user
create or replace procedure procedure_create_user(
  _user_name VARCHAR(64),
  _user_url VARCHAR(64),
  _user_email VARCHAR(64),
  _user_password VARCHAR(150)
)
language plpgsql    
as $$
begin
  insert into users(name, url, email, password) values (
    _user_name, _user_url, _user_email, _user_password
  );
end;$$