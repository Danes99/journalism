// Import pre-installed modules
import Head from 'next/head'
import Link from 'next/link'

// Import custom components
import DateFormat from '../../components/date'
import Layout from '../../components/layout'

// Import CSS
import styles from './styles.module.css'

// Import Constants
// import { DAO_ENDPOINT_ARTICLE, DAO_ENDPOINT_USER } from '../../config/dao'

// Articles
// const article404 = { title: 'Article does not exist', content: '404 not found' }
// const article500 = { title: 'Server error', content: '502 Bad Gateway' }

const article = {
    article_id: 1,
    article_title: 'Test title 1',
    article_content: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.',
    article_created_at: '2021-03-30T17:57:08.023Z',
    article_updated_at: '2021-03-30T17:57:08.023Z',

    user_id: 1,
    user_name: 'User name',
    user_url: 'test'
}

// Get data from the API:
// This function is executed before the rendering of the HTML page.
// The return pass the data as parameters to the rendering function.
export async function getStaticProps({ params }) {

    // Return results
    return { props: { article } }
}

// Render Web Page:
// This function is executed after the function 'getServerSideProps'
// The function parameters are the return of 'getServerSideProps'
export default function Post({ article }) {
    return (
        <Layout>

            {/* HTML Page Head */}
            <Head>
                <title>{article.article_title}</title>
            </Head>

            <article>

                {/* Article Title */}
                <div className={styles.articleTitle}>{article.article_title}</div>

                {/* Article Author */}
                {article.user_name && article.user_url && (
                    <div className={styles.articleAuthor}>
                        by&nbsp;
                        <Link href={`/author/${article.user_url}`}>
                            <a>{article.user_name}</a>
                        </Link>
                    </div>
                )}

                {/* Article Updated At */}
                {article.article_updated_at && (
                    <div className={styles.articleUpdatedAt}>
                        Updated <DateFormat dateString={article.article_updated_at} />
                    </div>
                )}

                {/* <h2>Content</h2> */}

                {/* Article content */}
                <div className={styles.articleContent}>
                    {article.article_content}
                </div>

            </article>

        </Layout>
    )
}