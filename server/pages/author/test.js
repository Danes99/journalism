// Import pre-installed modules
import Head from 'next/head'
// import Link from 'next/link'

// Import custom components
import DateFormat from '../../components/date'
import Layout from '../../components/layout'

// Import CSS
import styles from './styles.module.css'

const user = {
    user_id: 1,
    user_name: 'User name',
    user_url: 'test',
    user_created_at: '2021-03-30T17:57:08.023Z',
    user_updated_at: '2021-03-30T17:57:08.023Z',
    user_description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.'
}

// Get data from the API:
// This function is executed before the rendering of the HTML page.
// The return pass the data as parameters to the rendering function.
export async function getStaticProps({ params }) {

    // Return results
    return { props: { user } }
}

// Render Web Page:
// This function is executed after the function 'getServerSideProps'
// The function parameters are the return of 'getServerSideProps'
export default function User({ user }) {
    return (
        <Layout>

            {/* HTML Page Head */}
            <Head>
                <title>{!user || !user.user_name ? 'User' : user.user_name}</title>
            </Head>

            <article>

                {/* Username */}
                <div className={styles.articleTitle}>{user.user_name}</div>

                {/* User Created At */}
                {user && user.user_created_at && (
                    <div className={styles.articleUpdatedAt}>
                        Joined <DateFormat dateString={user.user_created_at} />
                    </div>
                )}

                {/* User Updated At */}
                {user && user.user_updated_at && (
                    <div className={styles.articleUpdatedAt}>
                        Last updated <DateFormat dateString={user.user_updated_at} />
                    </div>
                )}

                {/* Article content */}
                {user && user.user_description && (
                    <div className={styles.articleContent}>
                        {user.user_description}
                    </div>
                )}

            </article>

        </Layout>
    )
}